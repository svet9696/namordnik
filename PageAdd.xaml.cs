﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Namordnik
{
    /// <summary>
    /// Логика взаимодействия для PageAdd.xaml
    /// </summary>
    public partial class PageAdd : Page
    {
        public Product _currentProduct = new Product();

        public PageAdd(Product selectedProduct)
        {
            InitializeComponent();
           
            ComboBoxType.ItemsSource = DB_NamordnikEntities.GetContext().Products.ToList().GroupBy(x => x.type_product).Select(y => y.First());


            if (selectedProduct != null)
            {
                _currentProduct = selectedProduct;
            }
            DataContext = _currentProduct;
        }

        private void AddImg_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog
            {
                InitialDirectory = "",
                Filter = "Image files (*.jpg,*.png,*.bmp)|*.jpg;*.png;*.bmp|All Files (*.*)|*.*"
            };
            if (dlg.ShowDialog() == true)
            {
                string selectedFileName = dlg.FileName;
                _currentProduct.img_product = File.ReadAllBytes(dlg.FileName);
                image.Source = new BitmapImage(new Uri(selectedFileName));

            }
           
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            StringBuilder errors = new StringBuilder();
           var currentProd = DB_NamordnikEntities.GetContext().Products.ToList();

            if (string.IsNullOrWhiteSpace(_currentProduct.name))
                errors.AppendLine("Название указано неверно");
            if (string.IsNullOrWhiteSpace(_currentProduct.article))
                errors.AppendLine("Артикул указан неверно");
            if (_currentProduct.min_price <= 0)
                errors.AppendLine("Цена указана неверно");
            if (_currentProduct.count <= 0)
                errors.AppendLine("Количество указано неверно");
            if (_currentProduct.type_product == null)
                errors.AppendLine("Выберите группу");
            if (_currentProduct.img_product == null){
                string imgPath = System.IO.Path.GetFullPath(@"C:\Users\Yaroslav\Desktop\УП\Namordnik\Namordnik\Resources\picture.png");
                Uri imgUri = new Uri(imgPath);
                ImageSource img = new BitmapImage(imgUri);
                image.Source = img;
                _currentProduct.img_product = File.ReadAllBytes(imgPath);

            }
            if (errors.Length > 0)
            {
                MessageBox.Show(errors.ToString());
                return;
            }
            if (_currentProduct.id == 0)
                DB_NamordnikEntities.GetContext().Products.Add(_currentProduct);

            try
            {
              

                DB_NamordnikEntities.GetContext().SaveChanges();
                MessageBox.Show("Информация сохранена!");
                Manager.MainFrame.GoBack();
                


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

      

    }
}
