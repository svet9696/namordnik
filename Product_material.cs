//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Namordnik
{
    using System;
    using System.Collections.Generic;
    
    public partial class Product_material
    {
        public int id { get; set; }
        public string name_product { get; set; }
        public string name_material { get; set; }
        public int count_material { get; set; }
        public Nullable<int> id_product { get; set; }
        public Nullable<int> id_material { get; set; }
    
        public virtual Material Material { get; set; }
        public virtual Product Product { get; set; }
    }
}
