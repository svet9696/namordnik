﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Namordnik
{
    /// <summary>
    /// Логика взаимодействия для Data_Grid_Page.xaml
    /// </summary>
    public partial class Data_Grid_Page : Page
    {
        public Data_Grid_Page()
        {
            InitializeComponent();
           var currentMaterial = DB_NamordnikEntities.GetContext().Materials.ToList();
            DG_Material.ItemsSource = currentMaterial;

        }
    }
}
