﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Namordnik
{
    public static class PagingExtensions
    {
        public static IEnumerable<Product> Page<Product>(this IEnumerable<Product> source, int page, int pageSize)
        {
            return source.Skip((page - 1) * pageSize).Take(pageSize);
        }

    }
}
