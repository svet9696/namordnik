﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Namordnik
{
    /// <summary>
    /// Логика взаимодействия для GeneralPage.xaml
    /// </summary>
    /// 
    public partial class GeneralPage : Page
    {
        public int Page = 1;
        public int PageCount = 0;
        public int CountElement= 0;

        public GeneralPage()
        {
            
            InitializeComponent();
            ComboFilter.ItemsSource = DB_NamordnikEntities.GetContext().Products.ToList().GroupBy(x => x.type_product).Select(y => y.First());
           
            UpdatePage();
        }
        public void UpdatePage(){
            var currentProd = DB_NamordnikEntities.GetContext().Products.ToList();
            currentProd = currentProd.Where(p => p.type_product.ToLower().Contains(ComboFilter.Text.ToLower())).ToList();

            currentProd = currentProd.Where(p => p.name.ToLower().Contains(TextSearch.Text.ToLower())|| p.article.ToLower().Contains(TextSearch.Text.ToLower()) || p.type_product.ToLower().Contains(TextSearch.Text.ToLower())).ToList();
            

            if (currentProd.Count > 20)
            {
                PageCount = (int)(Convert.ToDouble(currentProd.Count) / 20.0);
            }
            else PageCount = 1;


            if (currentProd.Count > 0)
            {
                CountElement = currentProd.Count / PageCount;
            }
            else MessageBox.Show("Нечего не найдено");


            if (PageCount<Page)
            {
                Page = 1;
            }
            if (Page == 1)
            {
                BtnPageBack.IsEnabled = false;

            }
            else BtnPageBack.IsEnabled = true;

            if (Page == PageCount && Page > 1)
            {
                BtnPageNext.IsEnabled = false;

            }
            else BtnPageNext.IsEnabled = true;

            if (ComboSort.SelectedIndex > 0) 
            {
                ComboBoxItem sortComboBoxItem = (ComboBoxItem)ComboSort.SelectedItem;
                if(sortComboBoxItem.Content.ToString() == "По убыванию"){
                    ListProduct.ItemsSource = PagingExtensions.Page(currentProd, Page, CountElement).ToList().OrderByDescending(p => p.min_price);
                }
                if (sortComboBoxItem.Content.ToString() == "По возрастанию")
                {
                    ListProduct.ItemsSource = PagingExtensions.Page(currentProd, Page, CountElement).ToList().OrderBy(p => p.min_price);
                }
            } 
                
            ListProduct.ItemsSource = PagingExtensions.Page(currentProd, Page, CountElement).ToList();
            NumberPage.Text = Page.ToString();
            CountPage.Text = PageCount.ToString();
        }
        public void PaginationNext()
        {


            Page++;
            UpdatePage();
        }
        public void PaginationBack()
            {

            if (Page == 1)
            {
                BtnPageNext.IsEnabled = false;

            }
            else if (Page != 1)
                {
                    BtnPageNext.IsEnabled = true;
                    Page--;
                }
                UpdatePage();
            }
        

        private void BtnPageNext_Click(object sender, RoutedEventArgs e)
        {
            PaginationNext();
           

        }

        private void BtnPageBack_Click(object sender, RoutedEventArgs e)
        {
            PaginationBack();
            
        }

        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            Manager.MainFrame.Navigate(new PageAdd(null));
        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            Manager.MainFrame.Navigate(new PageAdd((sender as Button).DataContext as Product));
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            var ProductRemoving = ListProduct.SelectedItems.Cast<Product>().ToList();
            if (MessageBox.Show($"Вы точно хотите удалить следующие {ProductRemoving.Count()} элементов", "Внимание",
            MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                try
                {
                    DB_NamordnikEntities.GetContext().Products.RemoveRange(ProductRemoving);
                    DB_NamordnikEntities.GetContext().SaveChanges();
                    MessageBox.Show("Данные удалены");

                    ListProduct.ItemsSource = DB_NamordnikEntities.GetContext().Products.ToList();
                    UpdatePage();
   
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message.ToString());

                }

            }
        }

        private void TextSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdatePage();

            
        }

        private void ComboFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdatePage();

        }

        private void ComboSort_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdatePage();
        }
    }
}
