﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Namordnik
{
    /// <summary>
    /// Логика взаимодействия для StartPage.xaml
    /// </summary>
    public partial class StartPage : Page
    {
        public StartPage()
        {
            InitializeComponent();
        }

        private void product_Click(object sender, RoutedEventArgs e)
        {
            Manager.MainFrame.Navigate(new GeneralPage());
        }

        private void MaterialBtn_Click(object sender, RoutedEventArgs e)
        {
            Manager.MainFrame.Navigate(new Data_Grid_Page());
        }
    }
}
